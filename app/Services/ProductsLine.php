<?php

namespace App\Services;

class ProductsLine
{
    /**
     * Верный пакет данных который нужно вернуть программе
     * @var array
     */
    public $resultPackageData = [];

    /**
     * Входящий пакет данных который передаться программе
     */
    private $originalPackageData = [];


    public function run($originalPackageData)
    {
        $this->originalPackageData = json_decode($originalPackageData, true);
        if (!$this->originalPackageData) {
            return;
        }
        $this->originalPackageData = array_column($this->originalPackageData, null, 'end');

        foreach ($this->originalPackageData as $originalPackageData) {
            $start = $originalPackageData['start'];

            if (!isset($this->originalPackageData[$start])) {
                $this->moveFirstResultPackageData($originalPackageData);
                $this->originalPackageData = array_column($this->originalPackageData, null, 'start');
                break;
            }
        }

        $this->constructionPoint();

        return $this->resultPackageDataInfo();
    }

    private function resultPackageDataInfo(): array
    {
        $result = [];
        foreach ($this->resultPackageData as $resultPackageData) {
            $result[] = "Из {$resultPackageData['start']} в {$resultPackageData['end']} сотрудник {$resultPackageData['Name']}";
        }

        return $result;
    }

    /**
     * Перемещаем первую точку продукта в $resultPackageData
     */
    private function moveFirstResultPackageData($originalPackageData)
    {
        $this->resultPackageData[] = $originalPackageData;
        unset($this->originalPackageData[$originalPackageData['end']]);
    }

    /**
     * Собираем точки в нужной последовательности
     */
    private function constructionPoint()
    {
        while ($this->originalPackageData) {
            $activePoint = end($this->resultPackageData);

            if (isset($this->originalPackageData[$activePoint['end']])) {
                $nextPoint = $this->originalPackageData[$activePoint['end']];
                unset($this->originalPackageData[$activePoint['end']]);

                $this->resultPackageData[] = $nextPoint;
            } else {
                $this->originalPackageData = [];
            }
        }
    }
}
