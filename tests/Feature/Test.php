<?php

namespace Tests\Feature;

use App\Services\ProductsLine;

class Test extends ClientTestCase
{
    public function test()
    {
        $originalPackageData = '[
    {
        "start": "Склад",
        "end": "Мойка",
        "Name": "Иван"
    },
    {
        "start": "Мойка",
        "end": "Комбайн для нарезки",
        "Name": "Мария"
    },
    {
        "start": "Комбайн для нарезки",
        "end": "Миксер",
        "Name": "Анна"
    },
    {
        "start": "Миксер",
        "end": "Термообработка",
        "Name": "Андрей"
    },
    {
         "start": "Термообработка",
         "end": "Упаковка",
         "Name": "Андрей"
     }
    ]';

        $productsLine = (new ProductsLine)->run($originalPackageData);

        self::assertIsArray($productsLine);
    }
}
